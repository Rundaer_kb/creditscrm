/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import $ from "jquery";
window.$ = $;

require('@fortawesome/fontawesome-free/js/brands');
require('@fortawesome/fontawesome-free/js/solid');
require('@fortawesome/fontawesome-free/js/fontawesome');
require('bootstrap');

import './styles/app.scss';
