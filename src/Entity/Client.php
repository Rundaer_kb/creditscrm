<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
#[ORM\Table(name: 'client')]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $surname;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $phone;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $email;

    #[ORM\Column(type: 'date')]
    private ?DateTimeInterface $dateOfBirth;

    #[ORM\Column(type: 'string', length: 10)]
    private ?string $sex;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $voivodeship;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $county;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $community;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $postcode;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $town;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $address;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $citizenship;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $education;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $maritalStatus;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $phone2;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $contractOfEmployment;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $contractTime;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $civilLawContracts;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $business;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $industry;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $jobPosition;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $jobPositionType;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $employer;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $employerType;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $employerAddress;

    // This is owner side. One Client can be owner of many properties
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Property::class, orphanRemoval: true)]
    private ArrayCollection|PersistentCollection $properties;

    // This is owner side. One Client can be included in many processes
    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Process::class, orphanRemoval: true)]
    private ArrayCollection|PersistentCollection $processes;

    // This is owner side. One Client can have many credits
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Credit::class, orphanRemoval: true)]
    private ArrayCollection|PersistentCollection $credits;

    // This is owning side. Many Clients can be connected with only ONE user
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'clients')]
    #[ORM\Column(name: 'user_app_id', type: 'integer')]
    private $userApp;

    #[Pure]
    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->processes = new ArrayCollection();
        $this->credits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateOfBirth(): ?DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getVoivodeship(): ?string
    {
        return $this->voivodeship;
    }

    public function setVoivodeship(string $voivodeship): self
    {
        $this->voivodeship = $voivodeship;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getCommunity(): ?string
    {
        return $this->community;
    }

    public function setCommunity(string $community): self
    {
        $this->community = $community;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCitizenship(): ?string
    {
        return $this->citizenship;
    }

    public function setCitizenship(string $citizenship): self
    {
        $this->citizenship = $citizenship;

        return $this;
    }

    public function getEducation(): ?string
    {
        return $this->education;
    }

    public function setEducation(string $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function setPhone2(string $phone2): self
    {
        $this->phone2 = $phone2;

        return $this;
    }

    public function getContractOfEmployment(): ?int
    {
        return $this->contractOfEmployment;
    }

    public function setContractOfEmployment(?int $contractOfEmployment): self
    {
        $this->contractOfEmployment = $contractOfEmployment;

        return $this;
    }

    public function getContractTime(): ?string
    {
        return $this->contractTime;
    }

    public function setContractTime(?string $contractTime): self
    {
        $this->contractTime = $contractTime;

        return $this;
    }

    public function getCivilLawContracts(): ?string
    {
        return $this->civilLawContracts;
    }

    public function setCivilLawContracts(string $civilLawContracts): self
    {
        $this->civilLawContracts = $civilLawContracts;

        return $this;
    }

    public function getBusiness(): ?bool
    {
        return $this->business;
    }

    public function setBusiness(?bool $business): self
    {
        $this->business = $business;

        return $this;
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(?string $industry): self
    {
        $this->industry = $industry;

        return $this;
    }

    public function getJobPosition(): ?string
    {
        return $this->jobPosition;
    }

    public function setJobPosition(?string $jobPosition): self
    {
        $this->jobPosition = $jobPosition;

        return $this;
    }

    public function getJobPositionType(): ?string
    {
        return $this->jobPositionType;
    }

    public function setJobPositionType(?string $jobPositionType): self
    {
        $this->jobPositionType = $jobPositionType;

        return $this;
    }

    public function getEmployer(): ?string
    {
        return $this->employer;
    }

    public function setEmployer(?string $employer): self
    {
        $this->employer = $employer;

        return $this;
    }

    public function getEmployerType(): ?string
    {
        return $this->employerType;
    }

    public function setEmployerType(?string $employerType): self
    {
        $this->employerType = $employerType;

        return $this;
    }

    public function getEmployerAddress(): ?string
    {
        return $this->employerAddress;
    }

    public function setEmployerAddress(?string $employerAddress): self
    {
        $this->employerAddress = $employerAddress;

        return $this;
    }

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection|Property
    {
        return $this->properties;
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setOwner($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getOwner() === $this) {
                $property->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Process[]
     */
    public function getProcesses(): Collection|Process
    {
        return $this->processes;
    }

    public function addProcesses(Process $processes): self
    {
        if (!$this->processes->contains($processes)) {
            $this->processes[] = $processes;
            $processes->setClient($this);
        }

        return $this;
    }

    public function removeProcesses(Process $processes): self
    {
        if ($this->processes->removeElement($processes)) {
            // set the owning side to null (unless already changed)
            if ($processes->getClient() === $this) {
                $processes->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Credit[]
     */
    public function getCredits(): Collection|Credit
    {
        return $this->credits;
    }

    public function addCredit(Credit $credit): self
    {
        if (!$this->credits->contains($credit)) {
            $this->credits[] = $credit;
            $credit->setOwner($this);
        }

        return $this;
    }

    public function removeCredit(Credit $credit): self
    {
        if ($this->credits->removeElement($credit)) {
            // set the owning side to null (unless already changed)
            if ($credit->getOwner() === $this) {
                $credit->setOwner(null);
            }
        }

        return $this;
    }

    public function getUserApp(): UserInterface|int
    {
        return $this->userApp;
    }

    public function setUserApp(UserInterface|int $user): self
    {
        $this->userApp = $user->getId();

        return $this;
    }
}
