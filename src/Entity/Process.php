<?php

namespace App\Entity;

use App\Repository\ProcessRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: ProcessRepository::class)]
#[ORM\Table(name: 'process')]
class Process
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $applicationSubmissionOn;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $preliminaryDecisionOn;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $creditSignOn;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $creditRunOn;

    #[ORM\Column(type: 'date', nullable: true)]
    private ?DateTimeInterface $propertyHandOverOn;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $applicationAmount;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $positiveDecision;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $status;

    // This is owning side. Many Clients can be connected into many Processes
    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'process')]
    private ?Client $client;

    // Process can have only one Property
    #[ORM\OneToOne(targetEntity: Property::class, cascade: ['persist', 'remove'])]
    private ?Property $property;

    // Process can have only one Credit
    #[ORM\OneToOne(targetEntity: Credit::class, cascade: ['persist', 'remove'])]
    private ?Credit $credit;

    // This is owning side. Many Users can be connected into many Processes
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'processes')]
    #[ORM\Column(name: 'user_app_id', type: 'integer')]
    private ?UserInterface $userApp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApplicationSubmissionOn(): ?DateTimeInterface
    {
        return $this->applicationSubmissionOn;
    }

    public function setApplicationSubmissionOn(?DateTimeInterface $applicationSubmissionOn): self
    {
        $this->applicationSubmissionOn = $applicationSubmissionOn;

        return $this;
    }

    public function getPreliminaryDecisionOn(): ?DateTimeInterface
    {
        return $this->preliminaryDecisionOn;
    }

    public function setPreliminaryDecisionOn(?DateTimeInterface $preliminaryDecisionOn): self
    {
        $this->preliminaryDecisionOn = $preliminaryDecisionOn;

        return $this;
    }

    public function getCreditSignOn(): ?DateTimeInterface
    {
        return $this->creditSignOn;
    }

    public function setCreditSignOn(?DateTimeInterface $creditSignOn): self
    {
        $this->creditSignOn = $creditSignOn;

        return $this;
    }

    public function getCreditRunOn(): ?DateTimeInterface
    {
        return $this->creditRunOn;
    }

    public function setCreditRunOn(?DateTimeInterface $creditRunOn): self
    {
        $this->creditRunOn = $creditRunOn;

        return $this;
    }

    public function getPropertyHandOverOn(): ?DateTimeInterface
    {
        return $this->propertyHandOverOn;
    }

    public function setPropertyHandOverOn(?DateTimeInterface $propertyHandOverOn): self
    {
        $this->propertyHandOverOn = $propertyHandOverOn;

        return $this;
    }

    public function getApplicationAmount(): ?int
    {
        return $this->applicationAmount;
    }

    public function setApplicationAmount(?int $applicationAmount): self
    {
        $this->applicationAmount = $applicationAmount;

        return $this;
    }

    public function getPositiveDecision(): ?int
    {
        return $this->positiveDecision;
    }

    public function setPositiveDecision(?int $positiveDecision): self
    {
        $this->positiveDecision = $positiveDecision;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getCredit(): ?Credit
    {
        return $this->credit;
    }

    public function setCredit(?Credit $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getUserApp(): ?UserInterface
    {
        return $this->userApp;
    }

    public function setUserApp(?UserInterface $user): self
    {
        $this->userApp = $user->getId();

        return $this;
    }
}
