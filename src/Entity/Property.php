<?php

namespace App\Entity;

use App\Repository\PropertyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: PropertyRepository::class)]
#[ORM\Table(name: 'property')]
class Property
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $market;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $buildingType;

    #[ORM\Column(type: 'integer')]
    private ?int $buildingStoreys;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $apartmentStorey;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $developer;

    #[ORM\Column(type: 'float')]
    private ?float $surface;

    #[ORM\Column(type: 'integer')]
    private ?int $rooms;

    #[ORM\Column(type: 'integer')]
    private ?int $builtOn;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $address;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $town;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $community;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $county;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $voivodeship;

    #[ORM\Column(type: 'boolean')]
    private ?bool $parking;

    // This is owning side. Many Proprties can be connected with one Client
    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'properties')]
    private ?Client $owner;

    // This is owning side. Many Properties can be assigned to one User
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'properties')]
    #[ORM\Column(name: 'user_app_id', type: 'integer')]
    private UserInterface|int $userApp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarket(): ?string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getBuildingStoreys(): ?int
    {
        return $this->buildingStoreys;
    }

    public function setBuildingStoreys(int $buildingStoreys): self
    {
        $this->buildingStoreys = $buildingStoreys;

        return $this;
    }

    public function getApartmentStorey(): ?int
    {
        return $this->apartmentStorey;
    }

    public function setApartmentStorey(?int $apartmentStorey): self
    {
        $this->apartmentStorey = $apartmentStorey;

        return $this;
    }

    public function getDeveloper(): ?string
    {
        return $this->developer;
    }

    public function setDeveloper(?string $developer): self
    {
        $this->developer = $developer;

        return $this;
    }

    public function getSurface(): ?float
    {
        return $this->surface;
    }

    public function setSurface(float $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getRooms(): ?int
    {
        return $this->rooms;
    }

    public function setRooms(int $rooms): self
    {
        $this->rooms = $rooms;

        return $this;
    }

    public function getBuiltOn(): ?int
    {
        return $this->builtOn;
    }

    public function setBuiltOn(int $builtOn): self
    {
        $this->builtOn = $builtOn;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getCommunity(): ?string
    {
        return $this->community;
    }

    public function setCommunity(string $community): self
    {
        $this->community = $community;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getVoivodeship(): ?string
    {
        return $this->voivodeship;
    }

    public function setVoivodeship(string $voivodeship): self
    {
        $this->voivodeship = $voivodeship;

        return $this;
    }

    public function getParking(): ?bool
    {
        return $this->parking;
    }

    public function setParking(bool $parking): self
    {
        $this->parking = $parking;

        return $this;
    }

    public function getOwner(): ?Client
    {
        return $this->owner;
    }

    public function setOwner(?Client $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUserApp(): UserInterface|int
    {
        return $this->userApp;
    }

    public function setUserApp(UserInterface|int $user): self
    {
        $this->userApp = $user->getId();

        return $this;
    }
}
