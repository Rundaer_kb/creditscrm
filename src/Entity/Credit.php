<?php

namespace App\Entity;

use App\Repository\CreditRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CreditRepository::class)]
#[ORM\Table(name: 'credit')]
class Credit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $bank;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $purpose;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $protection;

    #[ORM\Column(type: 'integer')]
    private ?int $gracePeriod;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $instalment;

    #[ORM\Column(type: 'integer')]
    private ?int $paymentDay;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $interest;

    #[ORM\Column(type: 'float')]
    private ?float $commission;

    #[ORM\Column(type: 'float')]
    private ?float $bankMargin;

    #[ORM\Column(type: 'integer')]
    private ?int $loanPeriod;

    #[ORM\Column(type: 'integer')]
    private ?int $ownContribution;

    #[ORM\Column(type: 'integer')]
    private ?int $amountRequested;

    #[ORM\Column(type: 'integer')]
    private ?int $amountReceived;

    #[ORM\Column(type: 'integer')]
    private ?int $appraisalPropertyValue;

    #[ORM\Column(type: 'integer')]
    private ?int $purchaseValue;

    #[ORM\Column(type: 'integer')]
    private ?int $finishingValue;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $finishStandard;

    #[ORM\Column(type: 'boolean')]
    private ?bool $excessPayment;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $earlyPrepaymentFee;

    #[ORM\Column(type: 'boolean')]
    private ?bool $compulsoryInsurance;

    #[ORM\Column(type: 'integer')]
    private ?int $lifeInsurance;

    // This is owning side. Many Credits can owned by One Client
    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'credits')]
    private ?Client $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(string $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getPurpose(): ?string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): self
    {
        $this->purpose = $purpose;

        return $this;
    }

    public function getProtection(): ?string
    {
        return $this->protection;
    }

    public function setProtection(string $protection): self
    {
        $this->protection = $protection;

        return $this;
    }

    public function getGracePeriod(): ?int
    {
        return $this->gracePeriod;
    }

    public function setGracePeriod(int $gracePeriod): self
    {
        $this->gracePeriod = $gracePeriod;

        return $this;
    }

    public function getInstalment(): ?string
    {
        return $this->instalment;
    }

    public function setInstalment(string $instalment): self
    {
        $this->instalment = $instalment;

        return $this;
    }

    public function getPaymentDay(): ?int
    {
        return $this->paymentDay;
    }

    public function setPaymentDay(int $paymentDay): self
    {
        $this->paymentDay = $paymentDay;

        return $this;
    }

    public function getInterest(): ?string
    {
        return $this->interest;
    }

    public function setInterest(string $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    public function getCommission(): ?float
    {
        return $this->commission;
    }

    public function setCommission(float $commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    public function getBankMargin(): ?float
    {
        return $this->bankMargin;
    }

    public function setBankMargin(float $bankMargin): self
    {
        $this->bankMargin = $bankMargin;

        return $this;
    }

    public function getLoanPeriod(): ?int
    {
        return $this->loanPeriod;
    }

    public function setLoanPeriod(int $loanPeriod): self
    {
        $this->loanPeriod = $loanPeriod;

        return $this;
    }

    public function getOwnContribution(): ?int
    {
        return $this->ownContribution;
    }

    public function setOwnContribution(int $ownContribution): self
    {
        $this->ownContribution = $ownContribution;

        return $this;
    }

    public function getAmountRequested(): ?int
    {
        return $this->amountRequested;
    }

    public function setAmountRequested(int $amountRequested): self
    {
        $this->amountRequested = $amountRequested;

        return $this;
    }

    public function getAmountReceived(): ?int
    {
        return $this->amountReceived;
    }

    public function setAmountReceived(int $amountReceived): self
    {
        $this->amountReceived = $amountReceived;

        return $this;
    }

    public function getAppraisalPropertyValue(): ?int
    {
        return $this->appraisalPropertyValue;
    }

    public function setAppraisalPropertyValue(int $appraisalPropertyValue): self
    {
        $this->appraisalPropertyValue = $appraisalPropertyValue;

        return $this;
    }

    public function getPurchaseValue(): ?int
    {
        return $this->purchaseValue;
    }

    public function setPurchaseValue(int $purchaseValue): self
    {
        $this->purchaseValue = $purchaseValue;

        return $this;
    }

    public function getFinishingValue(): ?int
    {
        return $this->finishingValue;
    }

    public function setFinishingValue(int $finishingValue): self
    {
        $this->finishingValue = $finishingValue;

        return $this;
    }

    public function getFinishStandard(): ?string
    {
        return $this->finishStandard;
    }

    public function setFinishStandard(string $finishStandard): self
    {
        $this->finishStandard = $finishStandard;

        return $this;
    }

    public function getExcessPayment(): ?bool
    {
        return $this->excessPayment;
    }

    public function setExcessPayment(bool $excessPayment): self
    {
        $this->excessPayment = $excessPayment;

        return $this;
    }

    public function getEarlyPrepaymentFee(): ?string
    {
        return $this->earlyPrepaymentFee;
    }

    public function setEarlyPrepaymentFee(string $earlyPrepaymentFee): self
    {
        $this->earlyPrepaymentFee = $earlyPrepaymentFee;

        return $this;
    }

    public function getCompulsoryInsurance(): ?bool
    {
        return $this->compulsoryInsurance;
    }

    public function setCompulsoryInsurance(bool $compulsoryInsurance): self
    {
        $this->compulsoryInsurance = $compulsoryInsurance;

        return $this;
    }

    public function getLifeInsurance(): ?int
    {
        return $this->lifeInsurance;
    }

    public function setLifeInsurance(int $lifeInsurance): self
    {
        $this->lifeInsurance = $lifeInsurance;

        return $this;
    }

    public function getOwner(): ?Client
    {
        return $this->owner;
    }

    public function setOwner(?Client $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
