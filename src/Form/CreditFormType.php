<?php

namespace App\Form;

use App\Entity\Credit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bank', ChoiceType::class, [
                'label' => 'Bank',
                'choices' => [
                    'ING' => 'ING',
                    'Pekao' => 'Pekao',
                    'BNP' => 'BNP',
                    'Millennium' => 'Millennium',
                ],
            ])
            ->add('purpose', TextType::class, [
                'label' => 'Cel',
            ])
            ->add('protection', TextType::class, [
                'label' => 'Zabezpieczenie',
            ])
            ->add('gracePeriod', IntegerType::class, [
                'label' => 'Deklarowana karencja',
            ])
            ->add('instalment', ChoiceType::class, [
                'label' => 'Raty',
                'choices' => [
                    'równe' => 'równe',
                ],
            ])
            ->add('paymentDay', IntegerType::class, [
                'label' => 'Dzień płatności raty',
            ])
            ->add('interest', ChoiceType::class, [
                'label' => 'Oprocentowanie',
                'choices' => [
                    'stałe' => 'stałe',
                    'zmienne' => 'zmienne',
                ],
            ])
            ->add('commision', IntegerType::class, [
                'label' => 'Prowizja za udzielenie kredytu',
            ])
            ->add('bankMargin', IntegerType::class, [
                'label' => 'Marża banku',
            ])
            ->add('loanPeriod', IntegerType::class, [
                'label' => 'Okres kredytowania',
            ])
            ->add('ownContribution', MoneyType::class, [
                'label' => 'Wkład własny',
            ])
            ->add('amountRequested', MoneyType::class, [
                'label' => 'Kwota wnioskowana',
            ])
            ->add('amountReceived', MoneyType::class, [
                'label' => 'Kwota udzielona',
            ])
            ->add('appraisalPropertyValue', MoneyType::class, [
                'label' => 'Wartość nieruchomości z wyceny',
            ])
            ->add('purchaseValue', MoneyType::class, [
                'label' => 'Cena zakupu',
            ])
            ->add('finishingValue', MoneyType::class, [
                'label' => 'Kwota na wykończenie',
            ])
            ->add('finishStandard', ChoiceType::class, [
                'label' => 'Standard wykończenia',
                'choices' => [
                    'niski' => 'niski',
                    'średni' => 'średni',
                    'wysoki' => 'wysoki',
                ],
            ])
            ->add('excessPayment', ChoiceType::class, [
                'label' => 'Czy planowana nadpłata',
                'choices' => [
                    'Tak' => true,
                    'Nie' => false,
                ],
            ])
            ->add('earlyPrepaymentFee', ChoiceType::class, [
                'label' => 'Opłata za wcześniejszą spłatę (w latach)',
                'choices' => [
                    'Tak' => true,
                    'Nie' => false,
                ],
            ])
            ->add('compulsoryInsurance', ChoiceType::class, [
                'label' => 'Czy obowiązkowe ubezpieczenie nieruchumości',
                'choices' => [
                    'Tak' => true,
                    'Nie' => false,
                ],
            ])
            ->add('lifeInsurance', IntegerType::class, [
                'label' => 'Czy obowiązkowe ubezpieczenie życia',
                'help' => 'W latach.',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Credit::class,
        ]);
    }
}
