<?php

namespace App\Form\Process;

use App\Entity\Client;
use App\Entity\Process;
use App\Entity\Property;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcessStartFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => function (Client $client) {
                    return sprintf('%s %s', $client->getName(), $client->getSurname());
                },
                'choice_value' => 'id',
            ])
            ->add('property', EntityType::class, [
                'class' => Property::class,
                'choice_label' => function (Property $property) {
                    return sprintf('%s %s', $property->getAddress(), $property->getTown());
                },
                'choice_value' => 'id',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Rozpocznij',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Process::class,
        ]);
    }
}
