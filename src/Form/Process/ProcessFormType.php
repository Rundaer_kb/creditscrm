<?php

namespace App\Form\Process;

use App\Entity\Process;
use App\Enum\Process as EProcess;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcessFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices' => EProcess::getProcessStatusesForForm(),
                'placeholder' => 'Wybierz',
                'label' => 'Status',
            ])
            ->add('applicationSubmissionOn', DateType::class, [
                'label' => 'Data złożenia wniosku',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('preliminaryDecisionOn', DateType::class, [
                'label' => 'Data decyzji wstępnej',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('creditSignOn', DateType::class, [
                'label' => 'Data podpisania umowy kredytowej',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('creditRunOn', DateType::class, [
                'label' => 'Data uruchomienia kredytu',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('propertyHandOverOn', DateType::class, [
                'label' => 'Data oddania lokalu',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('applicationAmount', IntegerType::class, [
                'label' => 'Liczba aplikacji',
                'required' => false,
            ])
            ->add('positiveDecision', IntegerType::class, [
                'label' => 'Liczba pozytywnych decyzji',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Process::class,
        ]);
    }
}
