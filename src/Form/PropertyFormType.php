<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Property;
use App\Enum\VoivodeshipEnum;
use App\Repository\ClientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class PropertyFormType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('market', ChoiceType::class, [
                'label' => 'Rynek',
                'choices' => [
                    'pierwotny' => 'pierwotny',
                    'wtórny' => 'wtórny',
                ],
            ])
            ->add('buildingType', ChoiceType::class, [
                'label' => 'Rodzaj zabudowy',
                'choices' => [
                    'wielorodzinna' => 'wielorodzinna',
                    'szeregowa' => 'szeregowa',
                    'działka' => 'działka',
                    'bliźniacza' => 'bliźniacza',
                ],
            ])
            ->add('buildingStoreys', IntegerType::class, [
                'label' => 'Liczba kondygnacji',
            ])
            ->add('apartmentStorey', IntegerType::class, [
                'label' => 'Kondygnacja lokalu',
            ])
            ->add('developer', TextType::class, [
                'label' => 'Developer',
            ])
            ->add('surface', IntegerType::class, [
                'label' => 'Powierzchnia w m2',
            ])
            ->add('rooms', IntegerType::class, [
                'label' => 'Liczba pokoi',
            ])
            ->add('builtOn', IntegerType::class, [
                'label' => 'Rok wybudowania',
                'attr' => [
                    'class' => 'maskBuiltOn',
                ],
            ])
            ->add('address', TextType::class, [
                'label' => 'Adres',
            ])
            ->add('voivodeship', ChoiceType::class, [
                'expanded' => false,
                'choices' => VoivodeshipEnum::getForForm(),
                'placeholder' => 'Wybierz',
                'label' => 'Wojewódźtwo',
            ])
            ->add('county', ChoiceType::class, [
                'choices' => ['Polska' => 'polska'],
                'label' => 'Kraj',
            ])
            ->add('community', TextType::class, [
                'label' => 'Gmina',
            ])
            ->add('town', TextType::class, [
                'label' => 'Miasto',
            ])
            ->add('parking', ChoiceType::class, [
                'choices' => [
                    'Nie' => false,
                    'Tak' => true,
                ],
            ])
            ->add('owner', EntityType::class, [
                'label' => 'Właściciel',
                'class' => Client::class,
                'choice_label' => function (Client $client) {
                    return sprintf('%s %s', $client->getName(), $client->getSurname());
                },
                'query_builder' => function (ClientRepository $c) {
                    return $c->createQueryBuilder('c')
                        ->where('c.userApp = :user_id')
                        ->setParameter('user_id', $this->security->getUser()->getId())
                        ->orderBy('c.name', 'ASC');
                },
                'choice_value' => 'id',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }
}
