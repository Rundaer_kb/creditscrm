<?php

namespace App\Form;

use App\Entity\Client;
use App\Enum\People;
use App\Enum\VoivodeshipEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class ClientFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Imię',
            ])
            ->add('surname', TextType::class, [
                'label' => 'Nazwisko',
            ])
            ->add('phone', TelType::class, [
                'attr' => [
                    'class' => 'maskTel',
                    'placeholder' => '000-000-000',
                ],
                'constraints' => [
                    new Regex(
                        '/^[0-9]*$/',
                        'Tylko cyfry 0-9'
                    ),
                ],
                'label' => 'Numer telefonu',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
            ])
            ->add('dateOfBirth', DateType::class, [
                'label' => 'Data urodzenia',
            ])
            ->add('sex', ChoiceType::class, [
                'expanded' => true,
                'choices' => People::getGendersForForm(),
                'label' => 'Płeć',
            ])
            ->add('postcode', TextType::class, [
                'attr' => [
                    'class' => 'maskPostcode',
                    'placeholder' => '00-000',
                ],
                'label' => 'Kod pocztowy',
            ])
            ->add('address', TextType::class, [
                'label' => 'Adres',
            ])
            ->add('voivodeship', ChoiceType::class, [
                'expanded' => false,
                'choices' => VoivodeshipEnum::getForForm(),
                'placeholder' => 'Wybierz',
                'label' => 'Wojewódźtwo',
            ])
            ->add('county', ChoiceType::class, [
                'choices' => ['Polska' => 'polska'],
                'label' => 'Kraj',
            ])
            ->add('community', TextType::class, [
                'label' => 'Gmina',
            ])
            ->add('town', TextType::class, [
                'label' => 'Miasto',
            ])
            ->add('citizenship', TextType::class, [
                'label' => 'Obywatelstwo',
            ])
            ->add('education', TextType::class, [
                'label' => 'Wykształcenie',
            ])
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => People::getMartialStatusesForForm(),
                'placeholder' => 'Wybierz',
                'label' => 'Stan cywilny',
            ])
            ->add('phone2', TelType::class, [
                'attr' => [
                    'class' => 'maskTel',
                    'placeholder' => '000-000-000',
                ],
                'constraints' => [
                    new Regex(
                        "/\d/",
                        'Tylko cyfry 0-9'
                    ),
                ],
                'label' => 'Drugi numer telefonu',
            ])
            ->add('contractOfEmployment', MoneyType::class, [
                'label' => 'Umowa o pracę',
                'required' => false,
            ])
            ->add('contractTime', TextType::class, [
                'label' => 'Czas umowy',
                'required' => false,
            ])
            ->add('civilLawContracts', TextType::class, [
                'label' => 'Umowy cywilnoprawne',
                'required' => false,
            ])
            ->add('business', CheckboxType::class, [
                'label' => 'Działalność gosporadcza',
                'required' => false,
            ])
            ->add('industry', TextType::class, [
                'label' => 'Branża pracodawcy',
                'required' => false,
            ])
            ->add('jobPosition', TextType::class, [
                'label' => 'Stanowisko',
                'required' => false,
            ])
            ->add('jobPositionType', TextType::class, [
                'label' => 'Typ stanowiska',
                'required' => false,
            ])
            ->add('employer', TextType::class, [
                'label' => 'Pracodawca',
                'required' => false,
            ])
            ->add('employerType', TextType::class, [
                'label' => 'Rodzaj miejsca pracy',
                'required' => false,
            ])
            ->add('employerAddress', TextType::class, [
                'label' => 'Adres pracodawcy',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Dodaj',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
