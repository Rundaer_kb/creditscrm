<?php

namespace App\Enum;

abstract class People
{
    public const MARTIAL_STATUS_MALE_MARRIED = 'żonaty';
    public const MARTIAL_STATUS_FEMALE_MARRIED = 'mężatka';
    public const MARTIAL_STATUS_MALE_SINGLE = 'kawaler';
    public const MARTIAL_STATUS_FEMALE_SINGLE = 'panna';
    public const MARTIAL_STATUS_MALE_DIVORCED = 'rozwodnik';
    public const MARTIAL_STATUS_FEMALE_DIVORCED = 'rozwódka';
    public const MARTIAL_STATUS_MALE_WIDOW = 'wdowiec';
    public const MARTIAL_STATUS_FEMALE_WIDOW = 'wdowa';

    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';

    public static function getGendersForForm()
    {
        return $gender = [
            'male' => self::GENDER_MALE,
            'female' => self::GENDER_FEMALE,
        ];
    }

    public static function getMartialStatusesForForm()
    {
        return $martial_statuses = [
            'żonaty' => self::MARTIAL_STATUS_MALE_MARRIED,
            'mężatka' => self::MARTIAL_STATUS_FEMALE_MARRIED,
            'kawaler' => self::MARTIAL_STATUS_MALE_SINGLE,
            'panna' => self::MARTIAL_STATUS_FEMALE_SINGLE,
            'rozwodnik' => self::MARTIAL_STATUS_MALE_DIVORCED,
            'rozwódka' => self::MARTIAL_STATUS_FEMALE_DIVORCED,
            'wdowiec' => self::MARTIAL_STATUS_MALE_WIDOW,
            'wdowa' => self::MARTIAL_STATUS_FEMALE_WIDOW,
        ];
    }
}
