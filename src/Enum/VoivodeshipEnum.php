<?php

namespace App\Enum;

abstract class VoivodeshipEnum
{
    public const VOIVODESHIP = [
        'dolnośląskie',
        'kujawsko-pomorskie',
        'lubelskie',
        'łódzkie',
        'małopolskie',
        'mazowieckie',
        'opolskie',
        'podkarpackie',
        'podlaskie',
        'pomorskie',
        'śląskie',
        'świętokrzyskie',
        'warmińsko-mazurskie',
        'wielkopolskie',
        'zachodniopomorskie'
    ];

    public static function getForForm()
    {
        $voivodeship = [];
        foreach (self::VOIVODESHIP as $v){
            $voivodeship[$v] = $v;
        }
        return $voivodeship;
    }
}