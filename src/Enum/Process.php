<?php

namespace App\Enum;

abstract class Process
{
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_INACTIVE = 'inactive';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_COMPLETED = 'completed';

    public static function getTranslation($status)
    {
        /* TODO: Create TWIG extension function / translations / DBtable with lang */
        switch ($status) {
            case self::STATUS_IN_PROGRESS:
                return 'W trakcie';
            case self::STATUS_INACTIVE:
                return 'Aktywny';
            case self::STATUS_ACTIVE:
                return 'Nie aktywny';
            case self::STATUS_COMPLETED:
                return 'Zakończony';
        }
    }

    public static function getProcessStatusesForForm()
    {
        return $statuses = [
            'Aktywny' => self::STATUS_ACTIVE,
            'W trakcie' => self::STATUS_IN_PROGRESS,
            'Nieaktywny' => self::STATUS_INACTIVE,
            'Zakończony' => self::STATUS_COMPLETED,
        ];
    }
}
