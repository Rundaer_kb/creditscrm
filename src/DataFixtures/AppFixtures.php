<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Enum\People;
use App\Enum\VoivodeshipEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;
    private $em;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('rundaer@gmail.com');
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, '12345678'));
        $admin->setRoles(['ROLE_ADMIN']);

        $credit = new User();
        $credit->setEmail('credit@test.pl');
        $credit->setPassword($this->passwordEncoder->encodePassword($credit, '12345678'));
        $credit->setRoles(['ROLE_CREDIT_ADVISOR']);

        $credit2 = new User();
        $credit2->setEmail('credit2@test.pl');
        $credit2->setPassword($this->passwordEncoder->encodePassword($credit2, '12345678'));
        $credit2->setRoles(['ROLE_CREDIT_ADVISOR']);

        $middleman = new User();
        $middleman->setEmail('lead@test.pl');
        $middleman->setPassword($this->passwordEncoder->encodePassword($middleman, '12345678'));
        $middleman->setRoles(['ROLE_MIDDLEMAN']);

        $manager->persist($admin);
        $manager->persist($credit);
        $manager->persist($credit2);
        $manager->persist($middleman);
        $manager->flush();

        $credit_rep = $this->em->getRepository(User::class)->findOneBy(['email' => 'credit@test.pl']);
        $credit2_rep = $this->em->getRepository(User::class)->findOneBy(['email' => 'credit2@test.pl']);

        $faker = Factory::create();
        for ($i = 0; $i < 10; ++$i) {

            $voivodeship = VoivodeshipEnum::VOIVODESHIP[array_rand(VoivodeshipEnum::VOIVODESHIP)];
            $matrial_statuses = People::getMartialStatusesForForm()[array_rand(People::getMartialStatusesForForm())];
            $genders = People::getGendersForForm()[array_rand(People::getGendersForForm())];

            $client = new Client();
            $client2 = new Client();

            $client
                ->setName($faker->firstName)
                ->setSurname($faker->lastName)
                ->setPhone($faker->phoneNumber)
                ->setEmail($faker->email)
                ->setDateOfBirth($faker->dateTimeBetween('-30 years', 'now'))
                ->setSex($genders)
                ->setVoivodeship($voivodeship)
                ->setCounty('poland')
                ->setCommunity('random')
                ->setTown($faker->address)
                ->setAddress($faker->streetAddress)
                ->setPostcode('30123')
                ->setCitizenship('polskie')
                ->setEducation('wyzsze')
                ->setMaritalStatus($matrial_statuses)
                ->setPhone2($faker->phoneNumber)
                ->setContractTime(rand(1, 10))
                ->setUserApp($credit_rep)
            ;

            $client2
                ->setName($faker->firstName)
                ->setSurname($faker->lastName)
                ->setPhone($faker->phoneNumber)
                ->setEmail($faker->email)
                ->setDateOfBirth($faker->dateTimeBetween('-30 years', 'now'))
                ->setSex($genders)
                ->setVoivodeship($voivodeship)
                ->setCounty('poland')
                ->setCommunity('random')
                ->setTown($faker->address)
                ->setAddress($faker->streetAddress)
                ->setPostcode('30123')
                ->setCitizenship('polskie')
                ->setEducation('wyzsze')
                ->setMaritalStatus($matrial_statuses)
                ->setPhone2($faker->phoneNumber)
                ->setContractTime(rand(1, 10))
                ->setUserApp($credit2_rep)
            ;

            $manager->persist($client);
            $manager->persist($client2);
        }
        $manager->flush();
    }
}
