<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    private string $message = 'Access denied';
    private string $back_route_name = 'dashboard_index';

    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $message_and_back_route = explode('|', $accessDeniedException->getMessage());

        $request->getSession()->getFlashBag()->add('error', $message_and_back_route[0]);

        $test = $this->router->generate($message_and_back_route[1]);

        return new RedirectResponse($test);
    }
}
