<?php

namespace App\Security\Voter;

use App\Entity\Property;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PropertyVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';
    private const ADD = 'add';

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::VIEW, self::EDIT, self::ADD])
            && $subject instanceof Property;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $return = match ($attribute) {
            self::EDIT, self::VIEW => $this->checkIfOwnerIsConnected($user, $subject),
            self::ADD => true,
        };

        return $return;
    }

    private function checkIfOwnerIsConnected(UserInterface $user, Property $property)
    {
        if ($property->getUserApp() !== $user->getId()) {
            return false;
        }

        return true;
    }
}
