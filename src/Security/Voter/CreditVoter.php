<?php

namespace App\Security\Voter;

use App\Entity\Credit;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CreditVoter extends Voter
{
    private const VIEW = 'view';
    private const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::VIEW, self::EDIT])
            && $subject instanceof \App\Entity\Credit;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $return = match ($attribute) {
            self::EDIT, self::VIEW => $this->checkIfOwnerIsConnected($user, $subject)
        };

        return $return;
    }

    private function checkIfOwnerIsConnected(UserInterface $user, Credit $credit)
    {
        if ($credit->getOwner()->getUserApp() !== $user->getId()) {
            return false;
        }

        return true;
    }
}
