<?php

namespace App\Controller\Admin;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * Class UsersController.
 *
 * @Breadcrumb({
 *     "label" = "Użytkownicy", "route" = "admin_users_index"
 * })
 */
#[Route(path: 'admin')]
class UserController extends AbstractController
{
    /**
     * List of all users.
     */
    #[Route(path: '/users', name: 'admin_users_index')]
    public function index(UserRepository $user_rep): Response
    {
        $users = $user_rep->findAllRolesExceptOne('ROLE_ADMIN');

        $users_json = $this->json(
            data: $users,
            context: ['ignored_attributes' => ['clients', 'properties', 'processes']]
        );

        return $this->render('admin_panel/users/index.html.twig', [
            'users' => $users_json->getContent(),
        ]);
    }

    /**
     * Add new user.
     *
     * @Breadcrumb({
     *     "label" = "Dodaj"
     * })
     */
    #[Route(path: '/users/add', name: 'admin_users_add')]
    public function addUser(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        UserAuthenticator $authenticator
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $this->addFlash('success', 'Użytkownik został dodany');
        }

        return $this->render('security/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
