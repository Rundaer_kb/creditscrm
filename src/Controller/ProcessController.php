<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\Credit;
use App\Entity\Process;
use App\Form\Process\ProcessFormType;
use App\Form\Process\ProcessStartFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProcessController.
 *
 * @Breadcrumb({
 *      "label" = "Procesy",
 *      "route" = "processes_index"
 * })
 */
class ProcessController extends AbstractController
{
    #[Route(path: '/processes', name: 'processes_index')]
    public function index(EntityManagerInterface $em): Response
    {
        $processes = $em->getRepository(Process::class)->findAll();
        foreach ($processes as $process) {
            $process->setStatus(\App\Enum\Process::getTranslation($process->getStatus()));
        }

        $processes_json = $this->json($processes);
        return $this->render('user_panel/process/index.html.twig', [
            'processes' => $processes_json->getContent(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Podglad"
     * })
     *
     * @return Response
     */
    #[Route(path: '/processes/preview/{id}', name: 'processes_preview')]
    public function previewProcessAction(Process $process)
    {
        $process->setStatus(\Process::getTranslation($process->getStatus()));

        return $this->render('user_panel/process/preview.html.twig', ['process' => $process]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Dodaj"
     * })
     *
     * @return RedirectResponse|Response
     */
    #[Route(path: '/processes/add', name: 'processes_add')]
    public function addProcessAction(Request $request)
    {
        $process = new Process();
        $form = $this->createForm(ProcessStartFormType::class, $process);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $process->setStatus(\Process::STATUS_IN_PROGRESS);
                $em = $this->getDoctrine()->getManager();
                $em->persist($process);
                $em->flush();

                $this->addFlash('success', 'Proces został rozpoczęty');

                return $this->redirectToRoute('processes_preview', ['id' => $process->getId()]);
            }
        }

        return $this->render('user_panel/process/start.html.twig', [
            'process' => $process,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Edytuj"
     * })
     *
     * @return RedirectResponse|Response
     */
    #[Route(path: '/processes/edit/{id}', name: 'processes_edit')]
    public function editProcessAction(Process $process, Request $request)
    {
        $form = $this->createForm(ProcessFormType::class, $process);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($process);
                $em->flush();

                $this->addFlash('success', 'Proces został edytowany');

                return $this->redirectToRoute('processes_preview', ['id' => $process->getId()]);
            }
        }

        return $this->render('user_panel/process/edit.html.twig', [
            'process' => $process,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Podgląd kredytu"
     * })
     *
     * @param Credit $credit
     * @return Response
     */
    #[Route(path: '/processes/credit-preview/{id}', name: 'processes_credit_preview')]
    public function previewProcessCreditAction(Credit $credit): Response
    {
        return $this->render('user_panel/credit/preview.html.twig', ['credit' => $credit]);
    }
}
