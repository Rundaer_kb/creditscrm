<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\Credit;
use App\Form\CreditFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreditController.
 *
 * @Breadcrumb({
 *     "label" = "Kredyty"
 * })
 */
class CreditController extends AbstractController
{
    /**
     * @Breadcrumb({
     *     "label" = "Dodaj"
     * })
     */
    #[Route(path: '/credits/add/{id_client}{id_process}', name: 'credits_add')]
    public function addCreditAction(Request $request, int $id_client, int $id_process): Response
    {
        $credit = new Credit();
        $form = $this->createForm(CreditFormType::class, $credit);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();

                $client = $em->getRepository('App:Client')->findOneBy(['id' => $id_client]);
                $process = $em->getRepository('App:Process')->findOneBy(['id' => $id_process]);

                $credit->setOwner($client);
                $process->setCredit($credit);

                $em->persist($credit);
                $em->persist($process);
                $em->flush();

                $this->addFlash('success', 'Kredyt został dodany');

                return $this->redirectToRoute('');
            }
        }

        return $this->render('user_panel/credit/add.html.twig', [
            'credit' => $credit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Podglad"
     * })
     *
     * @return Response
     */
    #[Route(path: '/credits/preview/{id}', name: 'credits_preview')]
    public function previewCreditAction(Credit $credit)
    {
        $this->denyAccessUnlessGranted('view', $credit, 'Podgląd niedostępny|dashboard_index');

        return $this->render('user_panel/credit/preview.html.twig', ['credit' => $credit]);
    }
}
