<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\Property;
use App\Form\PropertyFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyController.
 *
 * @Breadcrumb({
 *     "label" = "Nieruchomości", "route" = "properties_index"
 * })
 */
class PropertyController extends AbstractController
{
    #[Route(path: '/properties', name: 'properties_index')]
    public function index(EntityManagerInterface $em): Response
    {
        $properties = $em->getRepository(Property::class)->findAll();

        $properties_json = $this->json(data: $properties, context: ['ignored_attributes' => ['userApp', 'owner']]);
        return $this->render('user_panel/property/index.html.twig', [
            'properties' => $properties_json->getContent(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Podglad"
     * })
     *
     * @return Response
     */
    #[Route(path: '/properties/preview/{id}', name: 'properties_preview')]
    public function previewClientAction(Property $property, Request $request)
    {
        $this->denyAccessUnlessGranted('view', $property, 'Podgląd niedostępny|properties_index');

        return $this->render('user_panel/property/preview.html.twig', ['property' => $property]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Dodaj"
     * })
     */
    #[Route(path: '/properties/add', name: 'properties_add')]
    public function addPropertyAction(Request $request): Response
    {
        $property = new Property();
        $this->denyAccessUnlessGranted('add', $property, 'Podgląd niedostępny|properties_index');

        $form = $this->createForm(PropertyFormType::class, $property);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();

                $property->setUserApp($this->getUser());

                $em->persist($property);
                $em->flush();

                $this->addFlash('success', 'Nieruchomość została dodana');

                return $this->redirectToRoute('properties_index');
            }
        }

        return $this->render('user_panel/property/add.html.twig', [
            'property' => $property,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Edytuj"
     * })
     *
     * @return Response
     */
    #[Route(path: '/properties/edit/{id}', name: 'properties_edit')]
    public function editPropertyAction(Property $property, Request $request)
    {
        $this->denyAccessUnlessGranted('view', $property, 'Podgląd niedostępny|properties_index');

        $form = $this->createForm(PropertyFormType::class, $property);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($property);
                $em->flush();

                $this->addFlash('success', 'Nieruchomość została edytowana');

                return $this->redirectToRoute('properties_index');
            }
        }

        return $this->render('user_panel/property/edit.html.twig', ['form' => $form->createView()]);
    }
}
