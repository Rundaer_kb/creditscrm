<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Entity\Client;
use App\Entity\User;
use App\Form\ClientFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserClientController.
 *
 * @Breadcrumb({
 *     "label" = "Klienci", "route" = "clients_index"
 * })
 */
class ClientController extends AbstractController
{
    #[Route(path: '/clients', name: 'clients_index')]
    public function index(EntityManagerInterface $em): Response
    {
        $clients = $em->getRepository(Client::class)->findBy(['userApp' => $this->getUser()->getId()]);
        $clients_json = $this->json(
            data: $clients,
            context: ['ignored_attributes' => ['properties']]
        );
        return $this->render('user_panel/client/index.html.twig', [
            'clients' => $clients_json->getContent(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Podglad"
     * })
     *
     * @return Response
     */
    #[Route(path: '/clients/preview/{id}', name: 'clients_preview')]
    public function previewClientAction(UserInterface $user, Client $client, Request $request)
    {
        /* TODO: find way to implement back route*/
        $this->denyAccessUnlessGranted('view', $client, 'Podgląd niedostępny|clients_index');

        $client->{'age'} = User::convertToAge($client->getDateOfBirth());

        return $this->render('user_panel/client/preview.html.twig', ['client' => $client]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Dodaj"
     * })
     *
     * @Route("/clients/add", name="clients_add")
     */
    #[Route(path: '/clients/add', name: 'clients_add')]
    public function addClientAction(Request $request): Response
    {
        $client = new Client();

        $form = $this->createForm(ClientFormType::class, $client);
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                $client->setUserApp($this->getUser());
                $em->persist($client);
                $em->flush();

                $this->addFlash('success', 'Klient został dodany');

                return $this->redirectToRoute('clients_index');
            }
        }

        return $this->render('user_panel/client/add.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Breadcrumb({
     *     "label" = "Edytuj"
     * })
     * @return Response
     */
    #[Route(path: '/clients/edit/{id}', name: 'clients_edit')]
    public function editClientAction(Client $client, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $client);

        $form = $this->createForm(ClientFormType::class, $client);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();

                $this->addFlash('success', 'Klient został edytowany');

                return $this->redirectToRoute('clients_index');
            }
        }

        return $this->render('user_panel/client/add.html.twig', ['form' => $form->createView()]);
    }
}
