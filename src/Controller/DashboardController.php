<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PropertyController.
 *
 * @Breadcrumb({
 *     "label" = "Dashboard", "route" = "dashboard_index"
 * })
 */
class DashboardController extends AbstractController
{

    #[Route(path: '/dashboard', name: 'dashboard_index')]
    public function index(): Response
    {
        return $this->render('user_panel/dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
